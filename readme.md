### Object Detection using Tensorflow & Nodejs

#### Prerequisites
- Python 3
- g++ (10.2.0)
- make (4.3)
- nodejs (12.7)

#### Steps to run
1. npm i
2. npx nodemon or npm start