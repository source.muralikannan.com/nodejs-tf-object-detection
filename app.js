const express = require('express');

const path = require('path');
// var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const multer = require('multer');
const uploadFormData = multer();
// const tf = require('@tensorflow/tfjs-node');

const uploadRouter = require('./routes/upload');
const odRouter = require('./routes/object-detection');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// app.use(express.static(path.join(__dirname, 'public')));
// app.set('view engine', 'html');

app.use('/', uploadRouter);
app.use('/test', (req, res) => {
    res.json('working');
});
app.use('/predict', uploadFormData.single('image'), odRouter);

module.exports = app;
