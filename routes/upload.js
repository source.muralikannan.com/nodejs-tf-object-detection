var express = require('express');
var router = express.Router();

router.get('/', (req, res, next) => {
    res.sendFile('upload.html', { root: './public' });
});

module.exports = router;