const express = require('express');
const router = express.Router();
const canvas = require('canvas');
const tf = require('@tensorflow/tfjs-node');
// const mobileNet = require('@tensorflow-models/mobilenet');
const cocoSSd = require('@tensorflow-models/coco-ssd');


let ObjectDetection = class {

    static async run(image) {
        let prediction = await this.predict(image);
        let outputImage = await this.generateCanvasImage(image, prediction.tfImage.shape[1], prediction.tfImage.shape[0], prediction.predictions)
        return { prediction: prediction, image: outputImage };
    }

    static async predict(image) {
        let model = await cocoSSd.load();
        image = tf.node.decodeImage(new Uint8Array(image));
        // Classify the image.
        // var predictions = await model.classify(image);
        let predictions = await model.detect(image);
        return { predictions: predictions, tfImage: image };
    }

    static async generateCanvasImage(imageData, width, height, predictions) {
        return new Promise((resolve, reject) => {
            let sourceImage = new canvas.Image;
            sourceImage.src = imageData;
            // console.log(sourceImage);
            let imgCanvas = canvas.createCanvas(width, height);
            let context = imgCanvas.getContext("2d");
            context.drawImage(sourceImage, 0, 0, width, height);
            predictions.forEach((prediction) => {
                let coordinates = {};
                coordinates.x = prediction.bbox[0];
                coordinates.y = prediction.bbox[1];
                coordinates.width = prediction.bbox[2];
                coordinates.height = prediction.bbox[3];

                // let canvasOverlay = imgCanvas;
                // let context = canvasOverlay.getContext('2d');
                context.beginPath();
                context.fillStyle = 'yellow';
                context.rect(coordinates.x, coordinates.y, coordinates.width, coordinates.height);
                context.lineWidth = 2;
                context.strokeStyle = 'yellow';
                context.stroke();
                context.fillText(prediction.class, coordinates.x, (coordinates.y - 5));
                context.font = "bold 50px";
            });
            resolve(imgCanvas.toDataURL());
        });
    }
}


router.post('/', async (req, res, next) => {
    let image = req.file.buffer;
    let result = await ObjectDetection.run(image);
    res.json(result)
    // const result = await predict(image);
    // console.log(result.predictions[0].bbox);
    // const outputImage = await generateCanvasImage(image, result.tfImage.shape[1], result.tfImage.shape[0], result.predictions);
    // res.json({ prediction: result.predictions, image: outputImage });
    res.end();
});

module.exports = router;